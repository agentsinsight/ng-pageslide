(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['angular'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('angular'));
    } else {
        factory(root.angular);
    }
}(this, function (angular) {
    function _generateNumber(min, max) {
        min = typeof min === 'number' ? min : 0;
        min = typeof min === 'number' ? min : 100;

        return ~~(Math.random() * (max - min + 1)) + min;
    }

    function _generateUuid() {
        return "00000-00000-00000-00000-00000".replace(/0/g, function () {
            return _generateNumber(0, 15).toString(16);
        });
    }

    function Controller($document, $element, $scope, $timeout, $transitions) {
        angular.extend(this, {
            $document: $document,
            $element: $element,
            $scope: $scope,
            $timeout: $timeout
        });

        this.isOpen = false;
        this.initTimeout = null;
        this.uuid = _generateUuid();

        // Events
        $scope.$on('$locationChangeStart', function () {
            if (!this.autoClose) return;

            this._psClose();
        }.bind(this));

        $transitions.onStart({}, function () {
            if (!this.autoClose) return;
            this._psClose();
        }.bind(this));
    }
    Controller.$inject = ['$document', '$element', '$scope', '$timeout', '$transitions'];

    angular.extend(Controller.prototype, {
        $onInit: function () {
            this._updateState();

            this.sliderElem = this.$element[0];
            $(this.sliderElem).addClass(this.className);
            $(this.sliderElem).attr('id', this.uuid);

            this.bodyElem = (this.container ? document.getElementById(this.container) : null) || document.body;

            if (this.push) {
                this.bodyElem.style.position = 'absolute';
                this.bodyElem.style.transitionDuration = this.speed + 's';
                this.bodyElem.style.transitionProperty = 'top, bottom, left, right';
                this.bodyElem.style.webkitTransitionDuration = this.speed + 's';
            }

            if (this.container) {
                this.bodyElem.style.position = 'relative';
                this.bodyElem.style.overflow = 'hidden';
            }

            this.$timeout.cancel(this.initTimeout);
            this.initTimeout = this.$timeout(function () {
                if (this.sliderElem.tagName.toLowerCase() !== 'div' &&
                    this.sliderElem.tagName.toLowerCase() !== 'pageslide') {
                    throw new Error('Pageslide can only be applied to <div> or <pageslide> elements');
                }

                if (this.sliderElem.children.length === 0) {
                    throw new Error('You need to have content inside the <pageslide>');
                }

                this.sliderElem.style.height = this.size;
                this.sliderElem.style.position = 'fixed';
                this.sliderElem.style.transitionDuration = this.speed + 's';
                this.sliderElem.style.transitionProperty = 'top, bottom, left, right';
                this.sliderElem.style.webkitTransitionDuration = this.speed + 's';
                this.sliderElem.style.zIndex = this.zindex;

                if (this.container) {
                    this.sliderElem.style.position = 'absolute';
                }

                this.bodyElem.appendChild(this.sliderElem);

                this.$scope.$watch('$ctrl.psOpen', function (newValue, oldValue) {
                    if (newValue === oldValue) return;

                    if (!!newValue) {
                        this._psOpen();
                    } else {
                        this._psClose();
                    }
                }.bind(this));

                this._initSlider();
            }.bind(this), 0);
        },

        $onChanges: function () {
            this._updateState();
        },

        $onDestroy: function () {
            this.$timeout.cancel(this.initTimeout);
            this._setOpen(false);

            var element = document.getElementById(this.uuid);

            if (element)
                element.parentNode.removeChild(element);
        },

        _detachEvents: function () {
            this.$document.off('keydown.' + this.uuid);
            this.$document.off('touchstart.' + this.uuid + ' mousedown.' + this.uuid);
        },

        _handleKeyDown: function (e) {
            var ESC_KEY = 27;
            var key = e.keyCode || e.which;

            if (key === ESC_KEY) {
                this._psClose();

                // FIXME check with tests
                // http://stackoverflow.com/questions/12729122/angularjs-prevent-error-digest-already-in-progress-when-calling-scope-apply

                this.$timeout(function () {
                    this.$scope.$apply();
                }.bind(this));
            }
        },

        _initSlider: function () {
            switch (this.side) {
                case 'right':
                    this.sliderElem.style.width = this.size;
                    this.sliderElem.style.height = '100%';
                    this.sliderElem.style.top = '0px';
                    this.sliderElem.style.bottom = '0px';
                    break;
                case 'left':
                    this.sliderElem.style.width = this.size;
                    this.sliderElem.style.height = '100%';
                    this.sliderElem.style.top = '0px';
                    this.sliderElem.style.bottom = '0px';
                    break;
                case 'top':
                    this.sliderElem.style.height = this.size;
                    this.sliderElem.style.width = '100%';
                    this.sliderElem.style.left = '0px';
                    this.sliderElem.style.right = '0px';
                    break;
                case 'bottom':
                    this.sliderElem.style.height = this.size;
                    this.sliderElem.style.width = '100%';
                    this.sliderElem.style.left = '0px';
                    this.sliderElem.style.right = '0px';
                    break;
            }

            if (this.psOpen) {
                this._psOpen();
            } else {
                this._psClose();
            }
        },

        _onBodyClick: function (e) {
            if (!this.clickOutside || !this.isOpen) return;

            var target = e.touches && e.touches[0] || e.target;
            var bodyHasTarget = this.bodyElem.contains(target);
            var sliderHasTarget = this.sliderElem.contains(target);

            if (this.isOpen && bodyHasTarget && !sliderHasTarget) {
                this.psOpen = false;
                this.$scope.$apply();
            }
        },

        _psClose: function () {
            if (!this.bodyElem || !this.sliderElem || !this.isOpen) return;

            switch (this.side) {
                case 'right':
                    this.sliderElem.style.right = "-" + this.size;
                    if (this.push) {
                        this.bodyElem.style.right = '0px';
                        this.bodyElem.style.left = '0px';
                    }
                    break;
                case 'left':
                    this.sliderElem.style.left = "-" + this.size;
                    if (this.push) {
                        this.bodyElem.style.left = '0px';
                        this.bodyElem.style.right = '0px';
                    }
                    break;
                case 'top':
                    this.sliderElem.style.top = "-" + this.size;
                    if (this.push) {
                        this.bodyElem.style.top = '0px';
                        this.bodyElem.style.bottom = '0px';
                    }
                    break;
                case 'bottom':
                    this.sliderElem.style.bottom = "-" + this.size;
                    if (this.push) {
                        this.bodyElem.style.bottom = '0px';
                        this.bodyElem.style.top = '0px';
                    }
                    break;
            }

            this._setOpen(false);
            this._triggerCallback();
        },

        _psOpen: function () {
            if (!this.bodyElem || !this.sliderElem || this.isOpen) return;

            switch (this.side) {
                case 'right':
                    this.sliderElem.style.right = "0px";
                    if (this.push) {
                        this.bodyElem.style.right = this.size;
                        this.bodyElem.style.left = '-' + this.size;
                    }
                    break;
                case 'left':
                    this.sliderElem.style.left = "0px";
                    if (this.push) {
                        this.bodyElem.style.left = this.size;
                        this.bodyElem.style.right = '-' + this.size;
                    }
                    break;
                case 'top':
                    this.sliderElem.style.top = "0px";
                    if (this.push) {
                        this.bodyElem.style.top = this.size;
                        this.bodyElem.style.bottom = '-' + this.size;
                    }
                    break;
                case 'bottom':
                    this.sliderElem.style.bottom = "0px";
                    if (this.push) {
                        this.bodyElem.style.bottom = this.size;
                        this.bodyElem.style.top = '-' + this.size;
                    }
                    break;
            }

            this._setOpen(true);

            // Delay attaching events, as they get fired immediately
            this.$timeout(function () {
                if (this.keyListener) {
                    this.$document.on('keydown.' + this.uuid, this._handleKeyDown.bind(this));
                }

                if (this.clickOutside) {
                    this.$document.on('touchstart.' + this.uuid + ' mousedown.' + this.uuid, this._onBodyClick.bind(this));
                }
            }.bind(this));
        },

        _setBodyClass: function () {
            if (!this.bodyElem) return;

            var bodyClass = this.className + '-body';
            var bodyClassRe = new RegExp(bodyClass + '-closed|' + bodyClass + '-open');

            this.bodyElem.className = this.bodyElem.className.replace(bodyClassRe, '');

            var newBodyClassName = bodyClass + '-' + (this.isOpen ? 'open' : 'closed');

            if (this.bodyElem.className[this.bodyElem.className.length - 1] !== ' ') {
                this.bodyElem.className += ' ' + newBodyClassName;
            } else {
                this.bodyElem.className += newBodyClassName;
            }
        },

        _setOpen: function (open) {
            this.isOpen = open;
            this._setBodyClass();

            if (!open)
                this._detachEvents();
        },

        _triggerCallback: function () {
            if (this.isOpen) {
                var onOpenCallback = this.onOpen();
                if (typeof onOpenCallback === 'function') onOpenCallback();
            } else {
                var onCloseCallback = this.onClose();
                if (typeof onCloseCallback === 'function') onCloseCallback();
            }
        },

        _updateState: function () {
            this.className = this.psClass || 'ng-pageslide';
            this.container = this.psContainer || false;
            this.push = this.psPush === 'true' && !this.container;
            this.size = this.psSize || '300px';
            this.speed = this.psSpeed || '0.5';
            this.zindex = this.psZindex || 1000;

            this.autoClose = this.psAutoClose || false;
            this.bodyClass = this.psBodyClass || false;

            this.keyListener = this.psKeyListener === 'true';
            this.side = this.psSide || 'right';
        }
    });

    angular
        .module('pageslide-directive', [])
        .component('pageslide', {
            restrict: 'EA',
            bindings: {
                psOpen: '=?',
                clickOutside: '<psClickOutside',
                psAutoClose: '@',
                psBodyClass: '@',
                psClass: '@',
                psContainer: '@',
                psKeyListener: '@',
                psPush: '@',
                psSide: '@',
                psSize: '@',
                psSpeed: '@',
                psZindex: '@',
                onOpen: '&onopen',
                onClose: '&onclose'
            },
            controller: Controller
        });
}));